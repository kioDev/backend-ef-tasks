using ef_example;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

AppContext.SetSwitch("Npgsql.EnableLegacyTimestampBehavior", true);
//builder.Services.AddDbContext<TaskContext>(p => p.UseInMemoryDatabase("TaskDB"));
builder.Services.AddNpgsql<TaskContext>(builder.Configuration.GetConnectionString("DefaultConnection"));

var app = builder.Build();

app.MapGet("/", () => "Hello World!");

app.MapGet("/dbconnect", async ([FromServices] TaskContext dbContext) =>
{
    dbContext.Database.EnsureCreated();
    return Results.Ok("DataBase created in memory " + dbContext.Database.IsInMemory());
});

app.MapGet("/api/tasks", async ([FromServices] TaskContext DbContext) =>
{
    return Results.Ok(DbContext.Tasks);
});

app.MapGet("/api/tasksMedium", async ([FromServices] TaskContext DbContext) =>
{
    return Results.Ok(DbContext.Tasks.Include(t => t.Category).Where(c => c.PriorityTask == ef_example.Models.Priority.Medium));
});


app.MapPost("/api/tasks", async ([FromServices] TaskContext dbContext, [FromBody] ef_example.Models.Task task) => {
    task.TaskId = Guid.NewGuid();
    task.DateCreated = DateTime.Now;
    await dbContext.AddAsync(task);
    await dbContext.SaveChangesAsync();
    return Results.Ok();
});

app.MapPut("/api/tasks/{id}", async ([FromServices] TaskContext dbContext, [FromBody] ef_example.Models.Task task, [FromRoute] Guid id) => {

    var currentTask = dbContext.Tasks.Find(id);

    if (currentTask != null) {
        currentTask.CategoryId = task.CategoryId;
        currentTask.Title = task.Title;
        currentTask.PriorityTask = task.PriorityTask;
        currentTask.Description = task.Description;
        currentTask.IsCompleted = task.IsCompleted;

        await dbContext.SaveChangesAsync();
        return Results.Ok();
    }

    return Results.NotFound();
});

app.MapDelete("/api/tasks/{id}", async ([FromServices] TaskContext dbContext, [FromRoute] Guid id) => {

    var currentTask = dbContext.Tasks.Find(id);

    if (currentTask == null)
        return Results.NotFound("Task not found.");

    dbContext.Remove(currentTask);
    await dbContext.SaveChangesAsync();

    return Results.Ok("Removed!");
});

app.Run();
