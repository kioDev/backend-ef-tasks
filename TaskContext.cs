﻿
using Microsoft.EntityFrameworkCore;
using ef_example.Models;
using System.Threading;

namespace ef_example {
    public class TaskContext : DbContext
    {
        public DbSet<Category> Categories { get; set; }
        public DbSet<Models.Task> Tasks{ get; set; }

        public TaskContext(DbContextOptions<TaskContext> options) : base(options) {}

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            List<Category> categoriesInit = new List<Category>();
            categoriesInit.Add(new Category() { CategoryId = Guid.Parse("fe2de405-c38e-4c90-ac52-da0540dfb4ef"), Name = "Actividades pendientes", weight = 20, Description = "Description" });
            categoriesInit.Add(new Category() { CategoryId = Guid.Parse("fe2de405-c38e-4c90-ac52-da0540dfb402"), Name = "Actividades personales", weight = 50, Description = "Description" });


            modelBuilder.Entity<Category>(category =>
            {
                category.ToTable("Category");
                category.HasKey(c => c.CategoryId);
                category.Property(c => c.Name).HasMaxLength(50).IsRequired();
                category.Property(c => c.Description).HasMaxLength(200).IsRequired();
                category.Property(c => c.weight);
                category.HasData(categoriesInit);
            });

            List<Models.Task> tasksInit = new List<Models.Task>();
            tasksInit.Add(new Models.Task() { TaskId = Guid.Parse("fe2de405-c38e-4c90-ac52-da0540dfb410"), IsCompleted = false, CategoryId = Guid.Parse("fe2de405-c38e-4c90-ac52-da0540dfb4ef"), Description = "Description", PriorityTask = Priority.Medium, Title = "Pago de servicios publicos", DateCreated = DateTime.Now });
            tasksInit.Add(new Models.Task() { TaskId = Guid.Parse("fe2de405-c38e-4c90-ac52-da0540dfb411"), IsCompleted = false, CategoryId = Guid.Parse("fe2de405-c38e-4c90-ac52-da0540dfb402"), Description = "Description", PriorityTask = Priority.Low, Title = "Terminar de ver pelicula en netflix", DateCreated = DateTime.Now });

            modelBuilder.Entity<Models.Task>(task =>
            {
                task.ToTable("Task");
                task.HasKey(p => p.TaskId);
                task.HasOne(p => p.Category).WithMany(p => p.Tasks).HasForeignKey(p => p.CategoryId);
                task.Property(p => p.Title).IsRequired().HasMaxLength(200);
                task.Property(p => p.PriorityTask).IsRequired();
                task.Property(p => p.DateCreated).IsRequired();
                task.Property(p => p.IsCompleted).IsRequired();
                task.Ignore(p => p.Resume);
                task.HasData(tasksInit);

            });

        }
   

    }
}
