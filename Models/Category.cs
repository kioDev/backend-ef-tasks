//using System.ComponentModel.DataAnnotations;

using System.Text.Json.Serialization;

namespace ef_example.Models
{
    public class Category
    {
        public Guid CategoryId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public int weight { get; set; }

        [JsonIgnore]
        public virtual ICollection<Task> Tasks { get; set; }
    }
}
